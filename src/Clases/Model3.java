/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author LIZAMA
 */
public class Model3 {
    public double getW(double lambdaBarra, double L){
        double Wvalue = 0;
        Wvalue = L / lambdaBarra;
        return Wvalue;
    }
    
    public double getWq(double lambdaBarra, double Lq){
        double WqValue = 0;
        WqValue = Lq / lambdaBarra;
        return WqValue;
    }
    
    public double getL(double miu, double lambda, double capacidad){
        double Lvalue = 0;
        Lvalue = ((lambda / miu) / (1 - (lambda / miu))) - ((capacidad + 1) * (Math.pow(lambda/miu, capacidad +1)))/(1 - Math.pow(lambda / miu, capacidad + 1));
        return Lvalue;
    }
    
    public double getLq(double lambda, double miu, double L, double p0){
        double LqValue = 0;
        LqValue = L - (1 - p0);
        return LqValue;
    }
    
    public double getRho(double miu, double lambda, double servidores){
        double ro = 0;
        ro = lambda / (servidores * miu);
        return ro;
    }
    
    public double getEficiencia(double miu, double W){
        double eficiencia = 0;
        eficiencia = miu * W;
        return eficiencia;
    }
    
    public double getLambdaBarra(double lambda,  double miu, double capacidad, double pk){
        double lambdaBarra = 0;
        lambdaBarra = lambda * (1 - pk);
        return lambdaBarra;
    }
    
    public double getProbabilidad(double clientes, double capacidad, double miu, double lambda){
        double p = 0; 
        p = (1 - (lambda / miu)) / (1 - Math.pow(lambda / miu, capacidad + 1));
        if (clientes != 0){
            if(clientes <= capacidad){
                p = Math.pow(lambda / miu, clientes) * p;
            }else{
                p = 0;
            }
        }
        return p;
    }
}
