/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author LIZAMA
 */
public class Model1 {
    public double getW(double miu, double lambda){
        double Wvalue = 0;
        Wvalue = 1 / (miu - lambda);
        return Wvalue;
    }
    
    public double getWq(double miu, double lambda){
        double WqValue = 0;
        WqValue = lambda / (miu *(miu - lambda));
        return WqValue;
    }
    
    public double getL(double miu, double lambda){
        double Lvalue = 0;
        Lvalue = lambda / (miu - lambda);
        return Lvalue;
    }
    
    public double getLq(double lambda, double Wq){
        double LqValue = 0;
        LqValue = lambda * Wq;
        return LqValue;
    }
    
    public double getRho(double miu, double lambda){
        double ro = 0;
        ro = lambda / miu;
        return ro;
    }
    
    public double getEficiencia(double miu, double W){
        double eficiencia = 0;
        eficiencia = miu * W;
        return eficiencia;
    }
    
    public double getProbabilidad(double clientes, double miu, double lambda){
        double p = 0; 
        p = Math.pow(lambda / miu, clientes) * ((miu - lambda) / miu);
        return p;
    }
    
    public double getProbabilidadWT(double miu, double lambda, double tiempo){
        double pwt = 0;
        pwt = Math.pow(Math.E, (-(miu)*(1 - (lambda / miu)) * tiempo));
        return pwt;
    }
    
}
