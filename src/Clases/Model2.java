/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author LIZAMA
 */
public class Model2 {
    public double getW(double lambda, double L){
        double Wvalue = 0;
        Wvalue = L / lambda;
        return Wvalue;
    }
    
    public double getWq(double lambda, double Lq){
        double WqValue = 0;
        WqValue = Lq / lambda;
        return WqValue;
    }
    
    public double getL(double miu, double lambda, double LqValue){
        double Lvalue = 0;
        Lvalue = LqValue + (lambda / miu);
        return Lvalue;
    }
    
    public double getLq(double lambda, double miu, double p, double servidores){
        double LqValue = 0;
        LqValue = (Math.pow((lambda / miu), (servidores + 1)) * p) / (factorial(servidores - 1)* Math.pow((servidores - (lambda / miu)) , 2));
        return LqValue;
    }
    
    public double getRho(double miu, double lambda, double servidores){
        double ro = 0;
        ro = lambda / (servidores * miu);
        return ro;
    }
    
    public double getEficiencia(double miu, double W){
        double eficiencia = 0;
        eficiencia = miu * W;
        return eficiencia;
    }
    
    public double factorial (double numero) {
    if (numero==0)
        return 1;
    else
        return numero * factorial(numero-1);
    }
    
    public double getProbabilidad(double clientes, double servidores, double miu, double lambda){
        double p = 0; 
        double suma = 0;
        for(int i = 0; i < servidores; i++){
            suma += Math.pow(lambda / miu, i) * (1 / factorial(i));
        }
        suma += (Math.pow(lambda / miu, servidores)/ factorial(servidores))*(1 / (1- (lambda / (servidores * miu))));
        if(clientes == 0){
            p = 1 / suma;
        }else{
            if(clientes > servidores){
                p = (Math.pow(lambda, clientes) / (Math.pow(miu, clientes) * factorial(servidores) * Math.pow(servidores, (clientes - servidores)))) * (1 / suma);
            }else if(clientes <= servidores){
                p = (Math.pow(lambda, clientes) / (Math.pow(miu, clientes) * factorial(clientes))) * (1/suma);
            }
        }
        return p;
    }
    
    public double getProbabilidadWT(double miu, double lambda, double tiempo, double servidores, double p0){
        double pwt = 0;
        //pwt = Math.pow(Math.E, -(miu) * tiempo);
        //pwt = C * (1 + (B) * (A));
        pwt = Math.pow(Math.E, -(miu) * tiempo) * (1 + ((p0* Math.pow((lambda / miu), servidores))/((factorial(servidores))*(1-(lambda / (servidores *miu))))) * ((1 - Math.pow(Math.E, (-(miu)*tiempo*(servidores - 1 - (lambda / miu)))))/ (servidores - 1 - (lambda / miu))));
        System.out.println(pwt);
        return pwt;
    }
    
}
