/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author LIZAMA
 */
public class Model4 {
    public double getW(double lambdaBarra, double L){
        double Wvalue = 0;
        Wvalue = L / lambdaBarra;
        return Wvalue;
    }
    
    public double getWq(double lambdaBarra, double Lq){
        double WqValue = 0;
        WqValue = Lq / lambdaBarra;
        return WqValue;
    }
    
    public double getL(double miu, double lambda, double capacidad, double servidores, double Lq){
        double Lvalue = 0;
        double suma1 = 0, suma2 = 0;
        for (int i = 0; i < servidores - 1; i++) {
            suma1 += i * getProbabilidad(i, capacidad, servidores, miu, lambda);
        }
        for (int j = 0; j < servidores - 1; j++) {
            suma2 += getProbabilidad(j, capacidad, servidores, miu, lambda);
        }
        Lvalue = suma1 + Lq + servidores * ( 1 - suma2);
        return Lvalue;
    }
    
    public double getLq(double lambda, double miu, double p0, double servidores, double ro, double capacidad){
        double LqValue = 0;
        LqValue = ((p0 * (Math.pow(lambda / miu, servidores)) * ro) / (factorial(servidores) * Math.pow((1 - ro), 2))) * (1 - Math.pow(ro, capacidad - servidores) - (capacidad - servidores) * Math.pow(ro, capacidad - servidores) * (1 - ro));
        return LqValue;
    }
    
    public double getRho(double miu, double lambda, double servidores){
        double ro = 0;
        ro = lambda / (servidores * miu);
        return ro;
    }
    
    public double getEficiencia(double miu, double W){
        double eficiencia = 0;
        eficiencia = miu * W;
        return eficiencia;
    }
    
    public double getLambdaBarra(double lambda,  double miu, double capacidad, double pk){
        double lambdaBarra = 0;
        lambdaBarra = lambda * (1 - pk);
        return lambdaBarra;
    }
    
    public double factorial (double numero) {
    if (numero==0)
        return 1;
    else
        return numero * factorial(numero-1);
    }
    
    public double getProbabilidad(double clientes, double capacidad, double servidores,  double miu, double lambda){
        double p = 0; 
        double suma1 = 0, suma2 = 0;
        for (int i = 0; i <= servidores; i++) {
            suma1 += Math.pow(lambda / miu, i ) * (1 / factorial(i));
        }
        int inicio = (int) servidores + 1;
        for (int j = inicio; j <= capacidad; j++) {
            suma2 += Math.pow(lambda / (servidores * miu), j - servidores);
        }
        p = 1 / (suma1 + (Math.pow(lambda / miu, servidores) * (1 / factorial(servidores))) * suma2);        
        if (clientes != 0){
            if(clientes <= servidores - 1){
                p = (Math.pow(lambda / miu, clientes) / factorial(clientes)) * p;
            }else if (clientes <= capacidad){
                p = (Math.pow(lambda / miu, clientes) / (factorial(servidores) * Math.pow(servidores, clientes - servidores))) * p;
            }
            else{
                p = 0;
            }
        }
        return p;
    }
}
