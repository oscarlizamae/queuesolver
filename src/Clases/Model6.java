/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author LIZAMA
 */
public class Model6 {
    public double getLambdaBarra(double lambda,  double miu, double L, double size){
        double lambdaBarra = 0;
        lambdaBarra = lambda * (size - L);
        return lambdaBarra;
    }
    
    public double getW(double lambdaBarra, double L){
        double Wvalue = 0;
        Wvalue = L / lambdaBarra;
        return Wvalue;
    }
    
    public double getWq(double lambdaBarra, double Lq){
        double WqValue = 0;
        WqValue = Lq / lambdaBarra;
        return WqValue;
    }
    
    public double getL(double miu, double lambda, double size, double servidores, double Lq){
        double Lvalue = 0;
        double suma1 = 0, suma2 = 0;
        for (int i = 0; i < servidores - 1; i++) {
            suma1 += i * getProbabilidad(i, size, servidores, miu, lambda);
        }
        for (int j = 0; j < servidores - 1; j++) {
            suma2 += getProbabilidad(j, size, servidores, miu, lambda);
        }
        Lvalue = suma1 + Lq + servidores * ( 1 - suma2);
        return Lvalue;
    }
    
    public double getLq(double miu, double lambda, double size, double servidores){
        double LqValue = 0;
        for (int i = (int) servidores; i <= size ; i++) {
            LqValue += (i - servidores) * getProbabilidad(i, size, servidores, miu, lambda);
        }
        return LqValue;
    }
    
    public double getRho(double miu, double lambda, double servidores){
        double ro = 0;
        ro = lambda / (servidores * miu);
        return ro;
    }
    
    public double getEficiencia(double miu, double W){
        double eficiencia = 0;
        eficiencia = miu * W;
        return eficiencia;
    }
    
    public double factorial (double numero) {
    if (numero==0)
        return 1;
    else
        return numero * factorial(numero-1);
    }
    
    public double getProbabilidad(double clientes, double size, double servidores, double miu, double lambda){
        double p = 0; 
        double suma1 = 0, suma2 = 0;
        for (int i = 0; i <= servidores - 1; i++) {
            suma1 += (factorial(size) / (factorial(size - i)) * factorial(i)) * (Math.pow(lambda / miu, i));
        }
        int inicio = (int) servidores;
        for (int j = inicio; j <= size; j++) {
            suma2 += (factorial(size) / (factorial(size - j)) * factorial(servidores) * Math.pow(servidores, j - servidores)) * (Math.pow(lambda / miu, j));
        }
        p = 1 / suma1 + suma2;
        if(clientes > 0){
            if(clientes <= servidores)
                p = (factorial(size) / (factorial(size - clientes)) * factorial(clientes)) * (Math.pow(lambda / miu, clientes)) * p;
            else if(clientes <= size)
                p = (factorial(size) / (factorial(size - clientes)) * factorial(servidores) * Math.pow(servidores, clientes - servidores)) * (Math.pow(lambda / miu, clientes)) * p;
            else 
                p = 0;
        }
        return p;
    }
}
