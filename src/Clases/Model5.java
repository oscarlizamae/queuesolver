/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author LIZAMA
 */
public class Model5 {
    
    public double getLambdaBarra(double lambda,  double miu, double L, double size){
        double lambdaBarra = 0;
        lambdaBarra = lambda * (size - L);
        return lambdaBarra;
    }
    
    public double getW(double lambdaBarra, double L){
        double Wvalue = 0;
        Wvalue = L / lambdaBarra;
        return Wvalue;
    }
    
    public double getWq(double lambdaBarra, double Lq){
        double WqValue = 0;
        WqValue = Lq / lambdaBarra;
        return WqValue;
    }
    
    public double getL(double miu, double lambda, double p0, double size){
        double Lvalue = 0;
        Lvalue = size - ((miu / lambda) * (1 - p0));
        return Lvalue;
    }
    
    public double getLq(double miu, double lambda, double size){
        double LqValue = 0;
        for (int i = 1; i <= size ; i++) {
            LqValue += (i - 1) * getProbabilidad(i, size, miu, lambda);
        }
        return LqValue;
    }
    
    public double getRho(double miu, double lambda, double servidores){
        double ro = 0;
        ro = lambda / (servidores * miu);
        return ro;
    }
    
    public double getEficiencia(double miu, double W){
        double eficiencia = 0;
        eficiencia = miu * W;
        return eficiencia;
    }
    
    public double factorial (double numero) {
    if (numero==0)
        return 1;
    else
        return numero * factorial(numero-1);
    }
    
    public double getProbabilidad(double clientes, double size,  double miu, double lambda){
        double p = 0; 
        double suma1 = 0;
        for (int i = 0; i <= size; i++) {
            suma1 += (factorial(size) / factorial(size - i)) * (Math.pow(lambda / miu, i));
        }
        p = 1 / suma1;
        if(clientes != 0){
            p = (factorial(size) / factorial(size - clientes)) * (Math.pow(lambda / miu, clientes)) * p;
        }
        return p;
    }
}
